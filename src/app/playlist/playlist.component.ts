import { Component, OnInit, Input } from '@angular/core';
import { Playlist } from '../playlist';
import { PlayerService } from '../player.service';
import { duration } from '../util';

@Component({
  selector: 'app-playlist',
  templateUrl: './playlist.component.html',
  styleUrls: ['./playlist.component.css']
})
export class PlaylistComponent implements OnInit {
  @Input() playlist: Playlist;
  @Input() showCovers: boolean;

  constructor(
    private playerService: PlayerService
  ) { }

  ngOnInit() {
  }

  isPlaying(i: number) {
    return this.playerService.isPlaying(this.playlist) && i === this.playerService.playingList.currentIndex;
  }

  play(i: number) {
    this.playlist.go(i);
    this.playerService.play(this.playlist);
  }

}
