import {Artist} from './artist';
import {Track} from './track';
import { Playlist } from './playlist';

export class Album {
    id: number;
    tracks: Track[];
    name: string;
    artist: Artist;
    coverId: number;
    description: string;
}