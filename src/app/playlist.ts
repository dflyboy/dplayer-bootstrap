import {Track} from './track';

export class Playlist {
    tracks: Track[];
    currentIndex: number;
    originUrl: string;

    constructor(tracks: Track[], origin?: string, index: number = 0) {
        this.tracks = tracks;
        this.currentIndex = index;
        this.originUrl = origin ? origin : '';
    }

    get current(): Track {
        return this.tracks[this.currentIndex];
    }

    // increase index and return track
    next(): Track {
        if(this.currentIndex < this.length-1) this.currentIndex++;
        return this.current;
    }

    prev(): Track {
        if(this.currentIndex > 0) this.currentIndex--;
        return this.current;
    }

    go(index: number) {
        this.currentIndex = index;
    }

    get length() {
        return this.tracks.length;
    }

}
