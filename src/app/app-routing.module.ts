import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResultsComponent } from './results/results.component';
import { RouterModule, Routes } from '@angular/router'
import { HomeComponent } from './home/home.component';
import { AlbumListComponent } from './album-list/album-list.component';

import { AlbumComponent } from './album/album.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistListComponent } from './artist-list/artist-list.component';

const routes: Routes = [
  { path: 'search/:query', component: ResultsComponent },
  { path: '', redirectTo: '/albums', pathMatch: 'full' },
  { path: 'albums', component: AlbumListComponent },
  { path: 'artists', component: ArtistListComponent},
  { path: 'album/:id', component: AlbumComponent},
  { path: 'artist/:id', component: ArtistComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { enableTracing:true })
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
