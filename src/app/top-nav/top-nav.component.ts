import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, RouterEvent, NavigationStart } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.css']
})
export class TopNavComponent implements OnInit {
  public isCollapsed = true;
  searchQuery: string;

  constructor(
    private router: Router,
    private location: Location
  ) { }

  ngOnInit() {
  }

  isActive(url: string): boolean {
    const rgx = new RegExp(url);
    return this.location && this.location.path().match(rgx) !== null;
  }

  onSearchChange(val) {
    this.searchQuery = val;
    if(this.searchQuery.length >= 3) this.search();
  }

  search() {
    this.router.navigateByUrl('/search/'+this.searchQuery);
  }

  stopBubble(e) {
    e.stopPropagation();
  }

}
