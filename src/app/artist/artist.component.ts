import { Component, OnInit } from '@angular/core';
import { Artist } from '../artist';
import { Playlist } from '../playlist';
import { PlayerService } from '../player.service';
import { ActivatedRoute } from '@angular/router';
import { Album } from '../album';
import {Location} from '@angular/common';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent implements OnInit {

  result: Artist;
  list: Playlist;
  title: string;

  constructor(
    private playerService: PlayerService,
    public route: ActivatedRoute,
    public location: Location
  ) { }

  getResults(): void {
    this.route.paramMap
    .subscribe(params => {
      this.playerService.getArtist(parseInt(params.get('id'))).subscribe(results => {
          this.result = results;
          this.result.tracks = results.tracks.map(track => {
            track.artist = new Artist();
            track.artist.id = results.id;
            track.artist.name = results.name;
            return track;
          })
          this.list = new Playlist(this.result.tracks, this.location.path());
          this.title = this.result.name;
        });
      this.title = params.get('name');
    });
  }

  ngOnInit() {
    this.getResults();
  }

}
