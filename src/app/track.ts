import {Artist} from './artist';
import {Album} from './album';

export class Track {
    _id: String;
    name: String;
    artist: Artist;
    album: Album;
    trackNo: Number;
    year: Number;
    coverId: Number;
    duration: Number;
}