import { Track } from "./track";

export class Artist {
    id: number;
    name: string;
    tracks: Track[];
    description: string;
}