import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public open: boolean;

  constructor() { }

  ngOnInit() {
    this.open = true;
  }

  public alertClose() {
    this.open = false;
  }

}
