import { Component, OnInit } from '@angular/core';
import { Artist } from '../artist';
import { PlayerService } from '../player.service';
import { Playlist } from '../playlist';

@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.css']
})
export class ArtistListComponent implements OnInit {
  artists: Artist[];
  artistTable: Artist[][];

  constructor(
    private playerService: PlayerService,
  ) { }

  ngOnInit() {
    this.playerService.getArtists().subscribe(artists => {
      this.artists = artists;
      this.artistTable = this.splitTable(artists, 4);
    })
  }

  play(id: number) {
    this.playerService.getArtist(id).subscribe(artist => {
      var list = new Playlist(artist.tracks);
      this.playerService.play(list);
    });
  }

  private splitTable(aList: Artist[], nCol: number): Artist[][] {
    var res: Artist[][] = [];
    for(let i=0; i<aList.length; i++) {
      const n: number = Math.floor(i/nCol);
      if(!res[n]) res[n] = [];
      res[n][i%nCol] = aList[i];
    }
    return res;
  }

}
