import { Component, OnInit } from '@angular/core';
import { Album } from '../album';
import { Playlist } from '../playlist';
import { PlayerService } from '../player.service';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  result: Album;
  list: Playlist;
  title: string;

  constructor(
    private playerService: PlayerService,
    public route: ActivatedRoute,
    public location: Location
  ) { }

  getResults(): void {
    this.route.paramMap
    .subscribe(params => {
      this.playerService.getAlbum(parseInt(params.get('id'))).subscribe(results => {
          this.result = results;
          this.result.tracks = results.tracks.map(track => {
            track.album = new Album();
            track.album.id = results.id;
            track.album.name = results.name;
            return track;
          })
          this.list = new Playlist(this.result.tracks, this.location.path());
          this.title = this.result.name;
        });
      this.title = params.get('name');
    });
  }

  ngOnInit() {
    this.getResults();
  }

}
