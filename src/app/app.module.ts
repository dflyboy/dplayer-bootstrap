import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import {NgxElectronModule} from 'ngx-electron';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import { ResultsComponent } from './results/results.component';
import { AlbumComponent } from './album/album.component';
import { PlayerComponent } from './player/player.component';
import { AlbumListComponent } from './album-list/album-list.component';
import { AppRoutingModule } from './app-routing.module';
import { PlaylistComponent } from './playlist/playlist.component';
import { ArtistComponent } from './artist/artist.component';
import { ArtistListComponent } from './artist-list/artist-list.component';
import { LazyloadDirective } from './lazyload.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TopNavComponent,
    ResultsComponent,
    AlbumComponent,
    PlayerComponent,
    AlbumListComponent,
    PlaylistComponent,
    ArtistComponent,
    ArtistListComponent,
    LazyloadDirective
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    HttpClientModule,
    NgxElectronModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
