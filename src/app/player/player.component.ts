import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { PlayerService } from '../player.service'
import { Track } from '../track';
import { Playlist } from '../playlist';
import { Title } from '@angular/platform-browser';
import { duration } from '../util';

import { ElectronService } from 'ngx-electron';

declare var fs: any;
declare var path: any;

@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

    constructor(
        private playerService: PlayerService,
        private titleService: Title,
        private electronService: ElectronService
    ) { }

    @ViewChild('player') jPlayer: ElementRef;
    @ViewChild('progBar', { read: ElementRef }) pBar: ElementRef;

    track: Track;
    playlist: Playlist;
    listOpen: boolean;
    playing: boolean;
    progress: number;
    duration: number;
    time: string;
    timeTotal: string;
    // waveform;

    ngOnInit() {
        this.titleService.setTitle('dPlayer');

        if (this.electronService.ipcRenderer) {
            this.electronService.ipcRenderer.on('control', (event, args) => {
                console.log(args);

                if (this.playlist) {
                    switch (args.action) {
                        case 'pause':
                            this.togPlayback(null);
                            break;
                        case 'next':
                            this.next();
                            break;
                        case 'prev':
                            this.prev();
                            break;
                    }
                }

            });
        }

        this.playerService.listen().subscribe(list => {
            // console.log('player recvd playlist');
            if (this.playlist != list) this.playlist = list;

            if (this.electronService.ipcRenderer) {
                this.electronService.ipcRenderer.send('track', this.playlist.current.artist.name + ' - ' + this.playlist.current.name);
                this.electronService.ipcRenderer.send('status', { playing: true });
            }
        });
    }

    next() {
        this.playlist.next();
        this.playerService.play(this.playlist);
        this.jPlayer.nativeElement.play();
    }

    prev() {
        this.playlist.prev();
        this.playerService.play(this.playlist);
        this.jPlayer.nativeElement.play();
    }

    togList() {
        this.listOpen = !this.listOpen;
    }

    togPlayback(e) {
        if (e) e.preventDefault();
        this.playing = this.jPlayer.nativeElement.paused;
        this.playing ? this.jPlayer.nativeElement.play() : this.jPlayer.nativeElement.pause();
        if (this.electronService.ipcRenderer) this.electronService.ipcRenderer.send('status', { playing: this.playing });
    }

    seekBar(event) {
        const bar = this.pBar.nativeElement;
        const offset = bar.getBoundingClientRect().left;
        const pos = event.pageX - offset;
        const width = bar.clientWidth;
        const m = pos / width;
        const time = m * this.duration;
        // console.log(pos, this.pBar.nativeElement.offsetLeft, width, m, time);
        this.jPlayer.nativeElement.currentTime = time;
    }

    updateProgress(): void {
        // this.progress = Math.floor((this.jPlayer.nativeElement.currentTime / this.jPlayer.nativeElement.duration) * 10000) / 100;
        const currentTime = this.jPlayer.nativeElement.currentTime;
        this.progress = (currentTime / this.duration) * 100;
        this.time = duration(currentTime);
    }

    updateDuration(): void {
        // this.progress = Math.floor((this.jPlayer.nativeElement.currentTime / this.jPlayer.nativeElement.duration) * 10000) / 100;
        this.duration = this.jPlayer.nativeElement.duration;
        this.timeTotal = duration(this.duration);
    }

    volChange(percent: number): void {
        this.jPlayer.nativeElement.volume = percent / 100;
    }

    playbackStalled(event) {

    }

}
