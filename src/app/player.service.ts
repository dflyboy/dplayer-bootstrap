import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Subject } from 'rxjs';

import { ElectronService } from 'ngx-electron';

import { Album } from './album';
import { Playlist } from './playlist';
import { Track } from './track';
import { Artist } from './artist';

const home = false;

const localUrl = 'http://192.168.178.54:5588/';
const remoteUrl = 'http://dflyboy420.dynu.net:5588/';
const baseUrl = home ? localUrl : remoteUrl;

// const baseUrl = 'http://127.0.0.1:5588/';

const albumsUrl = baseUrl + 'albums/';
const artistsUrl = baseUrl + 'artists/';
const tracksUrl = baseUrl + 'tracks/';
const coverUrl = baseUrl + 'cover/';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  private _listners = new Subject<Playlist>();
  public playingList: Playlist;

  private stallCount: number;

  listen(): Observable<Playlist> {
     return this._listners.asObservable();
  }

  play(list: Playlist) {
    console.log(list);
    this.playingList = list;
    this._listners.next(this.playingList);
  }

  isPlaying(list: Playlist): boolean {
    // console.log(list);
    if(! this.playingList) return false;
    return (list.originUrl !== '' && list.originUrl === this.playingList.originUrl ) || this.playingList === list;
  }
  
  constructor(
    private http: HttpClient,
  ) { }

  searchTracks(query: string): Observable<Track[]> {
    const url = tracksUrl + 'search';
    return this.http.get<Track[]>(url, {params: {q: query}});
  }

  searchAlbums(query: string): Observable<Track[]> {
    const url = `${albumsUrl}/search`;
    return this.http.get<Track[]>(url, {params: {q: query}});
  }

  searchArtists(query: string): Observable<Track[]> {
    const url = `${artistsUrl}/search`;
    return this.http.get<Track[]>(url, {params: {q: query}});
  }

  getAlbums(): Observable<Album[]> {
    return this.http.get<Album[]>(albumsUrl);
  }

  getArtists(): Observable<Artist[]> {
    return this.http.get<Artist[]>(artistsUrl);
  }

  getAlbum(id: number): Observable<Album> {
    return this.http.get<Album>(`${albumsUrl}${id}`);
  }

  getArtist(id: number): Observable<Artist> {
    return this.http.get<Artist>(`${artistsUrl}${id}`);
  }

  getUrl(trackId: number): string {
    return tracksUrl + trackId + '/stream';
  }

  getCoverUrl(track?: Track): String {
    return track ? coverUrl+track.coverId : coverUrl;
  }

  signalStalled(): void  {
    this.stallCount++;
  }

  getStallCount(): number {
    return this.stallCount;
  }
}
