import { Component, OnInit, Input } from '@angular/core';
import { Album } from '../album';
import {PlayerService} from '../player.service';
import { Playlist } from '../playlist';

@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.css']
})
export class AlbumListComponent implements OnInit {
  albums: Album[];
  albumTable: Album[][];

  constructor(
    private playerService: PlayerService,
  ) { }

  ngOnInit() {
    this.playerService.getAlbums().subscribe(albums => {
      this.albums = albums;
      this.albumTable = this.splitTable(albums, 4);
    });
  }

  playAlbum(id: number) {
    this.playerService.getAlbum(id).subscribe(album => {
      var list = new Playlist(album.tracks, '/album/'+id);
      this.playerService.play(list);
    });
  }

  private splitTable(aList: Album[], nCol: number): Album[][] {
    var res: Album[][] = [];
    for(let i=0; i<aList.length; i++) {
      const n: number = Math.floor(i/nCol);
      if(!res[n]) res[n] = [];
      res[n][i%nCol] = aList[i];
    }
    return res;
  }

}
