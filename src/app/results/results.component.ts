import { Component, OnInit } from '@angular/core';
import { PlayerService } from '../player.service';
import { ActivatedRoute } from '@angular/router';
import { Playlist } from '../playlist';
import {Location} from '@angular/common';


@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {
  list: Playlist;

  constructor(
    private playerService: PlayerService,
    public route: ActivatedRoute,
    public location: Location
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      const query = params.get('query');
      this.playerService.searchTracks(query).subscribe(result => {
        this.list = new Playlist(result, this.location.path());
      })
    })
  }

}
