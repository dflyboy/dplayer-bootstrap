export function duration(time: number) : string {
    const sec = String( Math.floor(time%60) );
    const secPadded = sec.padStart(2, '0');
    const min = Math.floor(time/60);

    return min + ':' + secPadded;
}